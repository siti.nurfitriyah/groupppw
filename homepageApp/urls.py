from django.conf.urls import url
from . import views
from django.urls import re_path, path, include

urlpatterns = [
	path('', views.index, name='index'),
	path('logout', views.logout, name='logout'),
	path('anginBogor', views.angin, name = 'anginBogor'),
	path('gempaLombok', views.lombok, name = 'gempaLombok'),
    path('gempaPalu', views.gempa, name = 'gempaPalu')
]
