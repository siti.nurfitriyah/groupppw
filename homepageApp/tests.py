from django.test import TestCase,Client
from django.urls import resolve
from .views import index, angin, gempa, lombok

# Create your tests here.
class CrowdFundingHomeTest(TestCase):
	def test_anginbogor_url_is_exist(self):
		response = Client().get('/anginBogor')
		self.assertEqual(response.status_code, 200)

	def test_anginbogor_using_anginBogor_template(self):
		self.assertTemplateUsed(Client().get('/anginBogor'), 'anginBogor.html')

	def test_anginbogor_using_angin_func(self):
		self.assertEqual(resolve('/anginBogor').func, angin)
	'''def test_homepage_contains_correct_code(self):
		response = Client().get('/index')
		self.assertContains(response, 'You can make them happy because you care')'''
		#self.assertContains(response, 'Penggalangan dana teratas')
		#self.assertContains(response, 'Berita')

	def test_index_url_homepage_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)

	def test_homepage_using_homepage_template(self):
		self.assertTemplateUsed(Client().get('/'), 'homePage.html')

	def test_homepage_using_index_func(self):
		self.assertEqual(resolve('/').func, index)

	def test_gempaPalu_url_is_exist(self):
		self.assertEqual(Client().get('/gempaPalu').status_code, 200)

	def test_gempaPalu_using_gempaPalu_template(self):
		self.assertTemplateUsed(Client().get('/gempaPalu'), 'gempaPalu.html')

	def test_gempaPalu_using_gempa_func(self):
		self.assertEqual(resolve('/gempaPalu').func, gempa)

	def test_gempalombok_url_is_exist(self):
		self.assertEqual(Client().get('/gempaLombok').status_code, 200)

	def test_gempalombok_using_gempaLombok_template(self):
		self.assertTemplateUsed(Client().get('/gempaLombok'), 'gempaLombok.html')

	def test_gempalombok_using_lombok_func(self):
		self.assertEqual(resolve('/gempaLombok').func, lombok)
