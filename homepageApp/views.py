
from django.shortcuts import render
from django.contrib.auth import logout as auth_logout
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect

# Create your views here.
def index (request):
    if request.user.is_authenticated:
        if 'name' not in request.session:
            request.session['name'] = request.user.first_name + " " + request.user.last_name
        if 'email' not in request.session:
            request.session['email'] = request.user.email
    return render (request, 'homePage.html')

def logout(request):
    request.session.flush()
    auth_logout(request)
    return HttpResponseRedirect('/')

def angin (request):
	return render (request, 'anginBogor.html')

def lombok (request):
	return render (request, 'gempaLombok.html')

def gempa (request):
	return render (request, 'gempaPalu.html')