from django import forms
from .models import *
class DateInput(forms.DateInput):
	input_type = 'date'

class FormRegister(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    nama_register = forms.CharField(label = 'Nama Lengkap', required=True, widget=forms.TextInput(attrs=attrs), max_length=30)
    tanggal_lahir_register = forms.DateField(label = 'Tanggal Lahir', required=True, widget=DateInput(attrs=attrs), input_formats = '%d/%m/%Y')
    email_register = forms.EmailField(label = 'Email', required=True, widget=forms.EmailInput(attrs=attrs), max_length=30)
    password_register = forms.CharField(label = 'Password', required=True, widget=forms.PasswordInput(attrs=attrs), max_length=15)
    class Meta:
    	model = FormRegisterModels
    
