from django.db import models
from django.utils import timezone
from datetime import datetime, date

class FormRegisterModels(models.Model):
	nama_register = models.CharField(max_length=30)
	tanggal_lahir_register = models.DateField()
	email_register = models.EmailField(unique=True, db_index=True)
	password_register = models.CharField(max_length=15)

	def __str__(self):
		return self.nama_register
