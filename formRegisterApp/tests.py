# Create your tests here.
from django.test import TestCase
from .views import *
from .models import *
from django.utils import timezone
from django.urls import resolve
from .views import formRegister, tampilan_pendaftaran
from .models import FormRegisterModels
from .forms import FormRegister


class FormRegisterTestCase(TestCase):
	def test_url_form_register(self):
		response = self.client.get('/formRegister/')
		self.assertEqual(response.status_code, 200)

	def setUp_register(self):
		return FormRegisterModels.objects.create(nama_register="Budi"
												,tanggal_lahir_register="2000-02-05"
												,email_register="budi@gmail.com"
												,password_register="12345" )

	def test_register_is_valid(self):
		new_register = self.setUp_register()
		self.assertEqual(new_register.nama_register, 'Budi')
		self.assertEqual(new_register.tanggal_lahir_register, '2000-02-05')
		self.assertEqual(new_register.email_register, 'budi@gmail.com')
		self.assertEqual(new_register.password_register, '12345')

	def test_url_after(self):
		response = self.client.get('/index/')
		self.assertEqual(response.status_code, 404)

	def test_nama_regis(self):
		namreg = FormRegisterModels(nama_register="Yudha")
		self.assertEqual(str(namreg), namreg.nama_register)

