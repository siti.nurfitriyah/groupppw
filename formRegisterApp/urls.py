from django.urls import re_path, path
from .views import formRegister, tampilan_pendaftaran
#url for app
urlpatterns = [
    re_path(r'formRegister/', formRegister, name='formRegister'),
    re_path(r'tampilan_pendaftaran/', tampilan_pendaftaran, name='tampilan_pendaftaran')
]
