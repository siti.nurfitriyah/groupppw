from django.db import models
from programApp.models import ProgramModels

class FormDonasiModels(models.Model):
	pilihan_program = ProgramModels.objects.all()
	namaProgram = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	jumlahUang = models.IntegerField()

# Create your models here.
