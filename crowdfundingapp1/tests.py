from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *

# Create your tests here.

# class TP1_Test(TestCase):
	# def test_website_url_is_exist(self):
	# 	response = Client().get('/')
	# 	self.assertEqual(response.status_code, 200) # If client access this, it would give 200 as a response.
	# def test_website_using_index_func(self):
	# 	found = resolve('/')
	# 	self.assertEqual(found.func, index)
	# def test_website_post_success_and_render_the_result(self):
	# 	response= Client().get('/')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn("Hello World!", html_response)

# Create your tests here.
class TP1UnitTest(TestCase):
	# ngecek url ada apa engga 
	def test_tp1_url_is_exist(self):
		response = self.client.get('/formDonasi/')
		self.assertEqual(response.status_code, 200)

	# def test_tampilan_donatur(self):
	# 	response = self.client.get('/tampilan_donatur/')
	# 	self.assertEqual(response.status_code, 302)

	'''def setUp_donation(self):
		return FormDonasiModels.objects.create(namaProgram="program satu"
												,namaLengkap="Yudha Pradipta Ramadan"
												,jumlahUang="50000")

	def test_donation_is_valid(self):
		new_donation = self.setUp_donation()
		self.assertEqual(new_donation.namaProgram, 'program satu')
		self.assertEqual(new_donation.namaLengkap, 'Yudha Pradipta Ramadan')
		self.assertEqual(new_donation.jumlahUang, '50000')

	def test_nama_donatur(self):
		namdon = FormDonasiModels(namaLengkap="Yudha Pradipta")
		self.assertEqual(str(namdon), namdon.namaLengkap)'''

	'''def test_tp1_has_welcome_text(self):
		response = Client().get('/formDonasi')
		text = response.content.decode('utf8')
		self.assertIn("Let's start goodness", text)'''

	# menggunakan fungsi status
	'''def test_tp1_use_donasi(self):
		found = resolve('/adddonasi')
		self.assertEqual(found.func, donasi)'''

	# bisa menambah status baru	
	# def test_tp1_can_create_jumlah_donasi(self):
	# 	# creating a new status
	# 	new_donasi = Donasi.objects.create(jumlah_donasi = 500000, transferNamaBank = "BNI", transferAtasNama = "Kayza", transferUntukProgram = "Rumah Untuk Donggala")

	# 	# retrieving
	# 	count_all_available_donasi = Donasi.objects.all().count()
	# 	self.assertEqual(count_all_available_donasi, 1)

	# kalo blank formnya
	# def test_form_validation_for_blank_items(self):
	# 	form = Donasi_Form(data={'nama program': '', 'nama lengkap': '', 'email': '', 'jumlah uang': ''})
	# 	self.assertFalse(form.is_valid())

	# test apakah sukses buat ngerender
	# def test_tp1_post_success_and_render_the_result(self):
	# 	test = 'Berbagi senyuman, ringankan beban, <br>dan ulurkan tangan mu untuk mereka!'
	# 	response_post = Client().post('/donasi', {'jumlah_donasi': 500, 'transferNamaBank': "BNI", 'transferAtasNama': "Kayza", 'transferUntukProgram': "Gempa Palu"})
	# 	self.assertEqual(response_post.status_code, 302)

		# response = Client().get('/donasi')
		# html_response = response.content.decode('utf8')
		# self.assertIn(test, html_response)

	# kalo error 
	# def test_tp1_post_error_and_render_the_result(self):
	# 	test = 'Mari Berbagi'
	# 	response = Client().post('/donasi',{})
	# 	self.assertEqual(response.status_code, 200)

	# 	response = Client().get('/donasi')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertNotIn('Rafika', html_response)
