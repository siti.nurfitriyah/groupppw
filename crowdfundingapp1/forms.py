from django import forms
from .models import FormDonasiModels
from programApp.models import ProgramModels

class FormDonasi(forms.Form):
	attrs = {
        'class': 'form-control'
    }
	#pilihan_program = ProgramModels.objects.all()
	namaProgram = forms.ChoiceField(label="Nama Program",required=True, widget=forms.TextInput(attrs=attrs))
	# namaLengkap = forms.CharField(label="Nama Lengkap", required=True, widget=forms.TextInput(attrs=attrs))
	jumlahUang = forms.IntegerField(label="Jumlah Uang",required=True, widget=forms.TextInput(attrs=attrs))
	class Meta:
		model = FormDonasiModels