from django.urls import re_path, path
from .views import *
#url for app
urlpatterns = [
    re_path(r'formDonasi/', formDonasi, name='formDonasi'),
    re_path(r'tampilan_donatur/', tampilan_donatur, name='tampilan_donatur'),
    re_path(r'history_donasi/', history_donasi, name='history_donasi')
]
