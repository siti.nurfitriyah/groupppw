from django.http import HttpResponseRedirect
from django.shortcuts import render
from formRegisterApp.models import FormRegisterModels
from programApp.models import ProgramModels
from .models import FormDonasiModels
from .forms import *
from django.shortcuts import render

response = {'author' : "Yudha" }
# Create your views here.

def formDonasi(request):
	response['form_sumbang'] = FormDonasi
	# response['nama'] = request.session['name']
	# response['email'] = request.session['email']
	return render(request, 'formDonasi.html', response)

def history_donasi(request):
	push_donasi = FormDonasiModels.objects.all()
	response['persumbangan'] = push_donasi
	#response['history_sumbangan'] = HistoryDonasi
	return render(request, 'donatur.html', response)

def tampilan_donatur(request):
 	sumbangan = FormDonasi(request.POST or None)
 	if (request.method == 'POST' and 'submit' in request.POST):
 		response['namaProgram'] = request.POST['namaProgram']if request.POST['namaProgram']!="" else "Anonymous"
 		response['email'] = request.user.email
 		response['jumlahUang'] = request.POST['jumlahUang']if request.POST['jumlahUang']!="" else "Anonymous"
 		terdonasi = FormDonasiModels(namaProgram=response['namaProgram'],email=response['email'], jumlahUang=response['jumlahUang'])
 		data_program = ProgramModels.objects.all()
 		for i in data_program:
 			if(i.nama_program==request.POST['namaProgram']):
 				terdonasi.save()
 				push_donasi = FormDonasiModels.objects.all()
 				response['persumbangan'] = push_donasi
 				donasi_html = 'donatur.html'
 				return render(request, donasi_html, response)
 			else:
 				return HttpResponseRedirect('/fungsiPushProgram/')
 	else:
 		return HttpResponseRedirect('/formDonasi/')

'''def ProgramView(request, judul):
    listDonatur = FormDonasiModels.objects.all()
    return render(request, 'gempaPalu.html', {'listDonatur': listDonatur, 'donatur':listDonatur.count()})'''
