# Generated by Django 2.1.1 on 2018-12-11 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crowdfundingapp1', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formdonasimodels',
            name='email',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='formdonasimodels',
            name='jumlahUang',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='formdonasimodels',
            name='namaLengkap',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='formdonasimodels',
            name='namaProgram',
            field=models.CharField(max_length=200),
        ),
    ]
