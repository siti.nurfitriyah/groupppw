"""TPver2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from formRegisterApp.views import formRegister as formRegister
from programApp.views import fungsiPushProgram as fungsiPushProgram
from homepageApp.views import index as index
from crowdfundingapp1.views import formDonasi as formDonasi
from django.conf.urls import url
from messageApp.views import *

urlpatterns = [
    re_path('admin/', admin.site.urls),
    re_path(r'^$', index, name='index'),
    re_path('', include('formRegisterApp.urls')),
    re_path('', include('programApp.urls')),

    re_path('', include('homepageApp.urls')),
    re_path('', include('crowdfundingapp1.urls')),

    # url(r'^homepageApp', include(('homepageApp.urls', 'homepageApp'), namespace='homepageApp')),
    re_path('', include('crowdfundingapp1.urls')),
    path('auth/', include('social_django.urls', namespace='social')),
    re_path('aboutus', include('messageApp.urls')),
    path('tampilanKomen/', tampilanKomen, name='tampilanKomen'),
    path('kumpulanKomen/', kumpulanKomen, name='kumpulanKomen')

]
