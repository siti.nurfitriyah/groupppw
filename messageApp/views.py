from django.shortcuts import render
from .forms import formPesan
from .models import masukan
from django.http import JsonResponse

def messageForm(request):
    response = {}
    response['form'] = formPesan()
    return render(request, "profile.html", response)

def aboutus(request):
    response = {}
    response['form'] = formPesan()
    return render(request, 'profile.html', response)

def tampilanKomen(request):
    username = request.user.first_name + " " + request.user.last_name
    komen = request.POST['message']
    komen_model = masukan(nama=username, pesan=komen)
    komen_model.save()
    waktu = komen_model.time.strftime("%b. %d, %Y, %I:%M %p")
    return JsonResponse({'nama':username, 'waktu':waktu, "pesan":komen})

def kumpulanKomen(request):
    kumpulan = masukan.objects.all().values("nama","time","pesan")
    banyakKomen= list(kumpulan)
    return JsonResponse(banyakKomen, safe=False)





