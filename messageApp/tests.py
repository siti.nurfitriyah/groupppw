from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import masukan
from .forms import formPesan
from .views import messageForm
# Create your tests here.

class aboutUsUnitTest(TestCase):
    def test_aboutUs_url_is_exist(self):
        response = Client().get('/aboutus/')
        self.assertEqual(response.status_code, 404)

    def test_model_check_items_in_model(self):
        masukan.objects.create(pesan='test',)
        counting_all_available_todo = masukan.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

#    def test_post_success(self):
#        request.session['name'] = 'name';
#        response_post = Client().post('/aboutus/', {'message': 'test',})
#        self.assertEqual(response_post.status_code, 200)

    # def test_page_using_daftardonatur_func(self):
    #     found = resolve('/aboutus/')
    #     self.assertEqual(found.func, aboutus)

    def test_no_login_no_form(self):
        response = Client().get('/aboutus/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<form>', html_response)

#    def test_if_login_form_yes(self):
#        request.session['name'] = 'name';
#        response = Client().get('/aboutus/')
#        html_response = response.content.decode('utf8')
#        self.assertIn('<form>', html_response)
