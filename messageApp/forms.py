from django import forms


class formPesan(forms.Form):
    search_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan pesan...',
    }
    message = forms.CharField(max_length = 300, label='', widget=forms.TextInput(attrs=search_attrs))
