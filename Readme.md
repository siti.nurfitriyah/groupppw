## KELOMPOK 12 | KELAS D

## Nama Anggotaa
1. Siti Nurfitriyah (1706984745)
2. Eldinta Yerusyah Taripar (1706984575)
3. Yudha Pradipta Ramadan (1706043424)

## Link Heroku 
[https://ppw-tugasanakpintar.herokuapp.com/](https://ppw-tugasanakpintar.herokuapp.com/)

## Status Website
[![pipeline status](https://gitlab.com/siti.nurfitriyah/groupppw/badges/master/pipeline.svg)](https://gitlab.com/siti.nurfitriyah/groupppw/commits/master)
[![coverage report](https://gitlab.com/siti.nurfitriyah/groupppw/badges/master/coverage.svg)](https://gitlab.com/siti.nurfitriyah/groupppw/commits/master)
