from django.apps import AppConfig


class ProgramappConfig(AppConfig):
    name = 'programApp'
