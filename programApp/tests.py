from django.test import TestCase
from .views import *
from .models import *

class ProgramTestCase(TestCase):
	def test_url_program(self):
		response = self.client.get('/fungsiPushProgram/')
		self.assertEqual(response.status_code, 200)

	def setUp_program(self):
		return ProgramModels.objects.create(nama_program="Bantu Mahasiswa Fasilkom"
												,deskripsi_program="Bantu mahasiswa fasilkom menyelesaikan tugas-tugas nya dan bantu mereka agar lulus semua matkul"
												,gambar_program="https://mk0learntocodew6bl5f.kinstacdn.com/wp-content/uploads/2016/11/Struggling-Programmer.jpg")

	def test_program_is_valid(self):
		new_program = self.setUp_program()
		self.assertEqual(new_program.nama_program, 'Bantu Mahasiswa Fasilkom')
		self.assertEqual(new_program.deskripsi_program, 'Bantu mahasiswa fasilkom menyelesaikan tugas-tugas nya dan bantu mereka agar lulus semua matkul')
		self.assertEqual(new_program.gambar_program, 'https://mk0learntocodew6bl5f.kinstacdn.com/wp-content/uploads/2016/11/Struggling-Programmer.jpg')
# Create your tests here.

